//
//  APIRouter.swift
//  CloudPhotos
//
//  Created by Fereizqo Sulaiman on 12/04/21.
//

import Foundation
import Alamofire

protocol APIConfiguration: URLRequestConvertible {
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: Parameters? { get }
    var header: String? { get }
}

enum APIRouter: APIConfiguration {
    case getCloudPhotos
    
    // Methods
    var method: HTTPMethod {
        switch self {
        case .getCloudPhotos:
            return .get
        }
    }
    
    // Path
    var path: String {
        switch self {
        case .getCloudPhotos:
            return "/test/photos.json"
        }
    }
    
    // Parameters
    var parameters: Parameters? {
        switch self {
        case .getCloudPhotos:
            return nil
        }
    }
    
    // Headers
    var header: String? {
        switch self {
        case .getCloudPhotos:
            return nil
        }
    }
    
    // URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try Constants.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // Method
        urlRequest.httpMethod = method.rawValue
        
        // Common headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        
        // Encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
    }
}
