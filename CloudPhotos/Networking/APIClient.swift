//
//  APIClient.swift
//  CloudPhotos
//
//  Created by Fereizqo Sulaiman on 12/04/21.
//

import Foundation
import Alamofire

class APIClient {
    @discardableResult
    
    private static func performRequest<T:Decodable>(route: APIRouter, decoder: JSONDecoder = JSONDecoder()) -> Future<T, Error> {
        return Future(operation: { completion in
            AF.request(route).responseDecodable(decoder: decoder, completionHandler: { (response: AFDataResponse<T>) in
                switch response.result {
                case .success(let value):
                    completion(.success(value))
                    
                case .failure(let error):
                    completion(.failure(error))
                }
                
            })
            
        })
    }
    
    // Get Photos
    static func getCloudPhotos() -> Future<[CloudPhoto], Error> {
        return performRequest(route: APIRouter.getCloudPhotos)
    }
    
}
