//
//  ImageTableViewCell.swift
//  CloudPhotos
//
//  Created by Fereizqo Sulaiman on 12/04/21.
//

import UIKit

class ImageTableViewCell: UITableViewCell {

    @IBOutlet weak var cloudImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var bookmarkButton: UIButton!
    
    var delegateCloudPhotoVC: CloudPhotosViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func bookmarkButton(_ sender: UIButton) {
        guard let delegate = delegateCloudPhotoVC else { return }
        delegate.onTapBookmark(cell: self)
    }
    
}
