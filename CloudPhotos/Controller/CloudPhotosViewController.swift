//
//  CloudPhotosViewController.swift
//  CloudPhotos
//
//  Created by Fereizqo Sulaiman on 12/04/21.
//

import UIKit
import HSPopupMenu

class CloudPhotosViewController: UIViewController {

    @IBOutlet weak var cloudPhotosTableView: UITableView!
    
    let searchController = UISearchController()
    let customTransitionDelegate = TransitionDelegate()
    var cloudPhotos: [CloudPhoto]?
    var filteredCloudPhotos: [CloudPhoto]?
    var categoryCloudPhotos: [String]?
    var isSelectFilter = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Do Get Request to collect the data
        getCloudPhotosData()
        
        // Initialize Tableview
        initTableView()
        
        // Initialize Search Bar
        initSearchController()

    }

    @IBAction func filterButton(_ sender: UIBarButtonItem) {
        // Null safety for categoryData
        guard let category = categoryCloudPhotos else { return }
        
        // Create category filter menu array
        var menuOption = [HSMenu]()
        for i in category {
            let menu = HSMenu(icon: nil, title: i)
            menuOption.append(menu)
        }
        
        // Show the category filter popup
        let popupMenu = HSPopupMenu(menuArray: menuOption, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-30, y: 80))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
    
    // Do API Request for Get the data
    func getCloudPhotosData() {
        // Show loading spinner
        Spinner.shared.showSpinner(onView: view)
        
        APIClient.getCloudPhotos().execute(onSuccess: { result in
            // Success do the API Request
            
            // Remove spinner
            Spinner.shared.removeSpinner()
            
            // Collect the data
            self.cloudPhotos = result
            // Get unique category and adding "All" category
            self.categoryCloudPhotos = result.map { $0.category_name }.unique
            self.categoryCloudPhotos?.append("All")
            
            // Reload tableview
            self.cloudPhotosTableView.reloadData()
            
        }, onFailure: { error in
            // Fail do the API Request
            
            // Remove spinner
            Spinner.shared.removeSpinner()
            
            // Show error message
            // Create new Alert
            let dialogMessage = UIAlertController(title: "Alert", message: "\(error.localizedDescription)", preferredStyle: .alert)
            
            // Create close button with action handler
            let close = UIAlertAction(title: "close", style: .default, handler: { (action) -> Void in
                
             })
            
            //Add close button to a dialog message
            dialogMessage.addAction(close)
            // Present Error Alert
            self.present(dialogMessage, animated: true, completion: nil)
        })
    }
    
    // Initiate Search Controller
    func initSearchController() {
        // Search Controller Setting
        definesPresentationContext = true
        searchController.loadViewIfNeeded()
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        
        // Attach Search Controller in Navigation
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        // Search Bar Setting and Delegate
        searchController.searchBar.returnKeyType = .done
        searchController.searchBar.enablesReturnKeyAutomatically = false
        searchController.searchBar.delegate = self
    }
    
    // Initiate TableView
    func initTableView() {
        // Register Xib Cell Files into TableView
        let cloudPhotosCellNib = UINib(nibName: "ImageTableViewCell", bundle: nil)
        cloudPhotosTableView.register(cloudPhotosCellNib, forCellReuseIdentifier: "imageTableViewCell")
        
        // TableView appearances setting
        cloudPhotosTableView.tableFooterView = UIView()
        
        // TableView delegate and datasource
        cloudPhotosTableView.delegate = self
        cloudPhotosTableView.dataSource = self
    }
}

//MARK:- TableView Delegate and Datasource
extension CloudPhotosViewController: UITableViewDelegate, UITableViewDataSource {
    
    // Return number of Row in TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Null safety for Original Data
        guard let data = cloudPhotos else { return 0 }
        
        // When filter is active (searched or categorized), return to Filtered Data count
        if searchController.isActive || isSelectFilter {
            guard let filteredData = filteredCloudPhotos else { return 0 }
            return filteredData.count
        }
        
        // When filter is not active, Return to Original Data count
        return data.count
    }
    
    // Cell setup in TableView
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Used the registered cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "imageTableViewCell") as! ImageTableViewCell
        
        // Delegate with ImageTableViewCell
        cell.delegateCloudPhotoVC = self
        
        // When filter is active (searched or categorized), displaying Filtered Data
        if searchController.isActive || isSelectFilter {
            guard let filteredData = filteredCloudPhotos else { return cell }
            cell.nameLabel.text = filteredData[indexPath.row].id
            cell.categoryLabel.text = filteredData[indexPath.row].category_name
            cell.cloudImage.load(urlString: filteredData[indexPath.row].url)
            cell.bookmarkButton.tintColor = filteredData[indexPath.row].isBookmarked ? UIColor.systemTeal : .lightGray
        }
        
        // When filter is not active, displaying Original Data
        else {
            guard let data = cloudPhotos else { return cell }
            cell.nameLabel.text = data[indexPath.row].id
            cell.categoryLabel.text = data[indexPath.row].category_name
            cell.cloudImage.load(urlString: data[indexPath.row].url)
            cell.bookmarkButton.tintColor = data[indexPath.row].isBookmarked ? UIColor.systemTeal : .lightGray
        }

        return cell
    }
    
    // Selecting Row in TableView
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Animate when deselectRow
        tableView.deselectRow(at: indexPath, animated: true)
        
        // Initiate Detail Cloud Photo VC
        guard let detailCloudPhotosVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailCloudPhotosViewController") as? DetailCloudPhotosViewController  else {
            return
        }
        
        // Setting for transition
        detailCloudPhotosVC.modalPresentationStyle = .fullScreen
        detailCloudPhotosVC.modalTransitionStyle = .crossDissolve
        detailCloudPhotosVC.transitioningDelegate = customTransitionDelegate
        transitioningDelegate = customTransitionDelegate
        
        // Delegate with detailCloudVC
        detailCloudPhotosVC.delegateCloudPhotoVC = self
        
        // Pass the Data to detailCloudVC
        // When filter is active (search or category), pass Filtered Data
        if searchController.isActive || isSelectFilter {
            guard let filteredData = filteredCloudPhotos else { return }
            detailCloudPhotosVC.cloudPhoto = filteredData[indexPath.row]
        }
        // When filter is not active, pass Original Data
        else {
            guard let data = cloudPhotos else { return }
            detailCloudPhotosVC.cloudPhoto = data[indexPath.row]
        }
        
        // Present Detail Image
        self.present(detailCloudPhotosVC, animated: true)
        
    }
    
}

//MARK:- SearchBarController Delegate
extension CloudPhotosViewController: UISearchBarDelegate, UISearchResultsUpdating {
    
    // Update when text in SearchBar changed
    func updateSearchResults(for searchController: UISearchController) {
        // Check search bar is'nt emppty
        if searchController.searchBar.text != nil {
            // Null safety for Original Data
            guard let data = cloudPhotos else { return }
            // Get the Filtered Data according to Text in Searchbar
            filteredCloudPhotos = data.filter({$0.id.lowercased().contains(searchController.searchBar.text!.lowercased())})
        }
        
        // Reload TableView
        cloudPhotosTableView.reloadData()
    }
}

//MARK:- FilterPopUp Menu Delegate
extension CloudPhotosViewController: HSPopupMenuDelegate {
    
    // Selecting PopUp Menu
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        
        // Null safety for Original Data and Category Data
        guard let data = cloudPhotos, let category = categoryCloudPhotos else { return }
        
        // Deactivate filter when select All
        if index == category.count - 1 {
            isSelectFilter = false
        }
        // Activate filter based on selected Category
        else {
            isSelectFilter = true
            filteredCloudPhotos = data.filter({$0.category_name.lowercased().contains(category[index].lowercased())})
        }
        
        // Reload tableview
        cloudPhotosTableView.reloadData()
        
    }
}

// MARK: - Delegate Function for Another ViewController
extension CloudPhotosViewController {
    // Delegate with TableViewCell
    // Tapping Bookmarks Button in TableViewCell
    func onTapBookmark(cell: UITableViewCell) {
        // Null safety for Index of Tapped row in TableView
        guard let indexPathTapped = cloudPhotosTableView.indexPath(for: cell) else { return }
        
        // When Selecting TableView From Filter State (Searched or Categorized)
        if searchController.isActive || isSelectFilter {
            // Null safety for Filtered Data
            guard let filteredData = filteredCloudPhotos else { return }
            
            // Save the current isBookmarked value
            let isBookmarked = filteredData[indexPathTapped.row].isBookmarked
            // Change the value reversely in Filtered Data
            filteredCloudPhotos![indexPathTapped.row].isBookmarked = !isBookmarked
            // Reload the tableview row
            cloudPhotosTableView.reloadRows(at: [indexPathTapped], with: .none)
            
            // Also, change the value of isBookmarked in Original Data
            if let originData = cloudPhotos!.enumerated().first(where: {$0.element.id == filteredData[indexPathTapped.row].id}) {
                cloudPhotos![originData.offset].isBookmarked = !isBookmarked
            }
        }
        
        // When Selecting TableView From Non-Filter State
        else {
            // Null safety for Original Data
            guard let data = cloudPhotos else { return }
            
            // Save the current isBookmarked value
            let isBookmarked = data[indexPathTapped.row].isBookmarked
            // Change the value reversely in Filtered Data
            cloudPhotos![indexPathTapped.row].isBookmarked = !isBookmarked
            // Reload the tableview row
            cloudPhotosTableView.reloadRows(at: [indexPathTapped], with: .none)
        }
        
    }
    
    // Delegate with DetailCloudPhotoVC
    // Tapping Bookmarks Button in DetailCloudPhotoVC
    func updateBookmarkData(photo: CloudPhoto) {
        // Get the isBookmarked value of selected photo
        let isBookmarked = photo.isBookmarked
        
        // Change the value of isBookmarked in Original Data
        if let originData = cloudPhotos!.enumerated().first(where: {$0.element.id == photo.id}) {
            cloudPhotos![originData.offset].isBookmarked = !isBookmarked
        }
        
        // Detect when using the Filter to Change the Filtered Data
        if searchController.isActive || isSelectFilter {
            if let filteredData = filteredCloudPhotos!.enumerated().first(where: {$0.element.id == photo.id}) {
                filteredCloudPhotos![filteredData.offset].isBookmarked = !isBookmarked
            }
        }
        
        // Reload the tableview
        cloudPhotosTableView.reloadData()
    }
    
}
