//
//  DetailCloudPhotosViewController.swift
//  CloudPhotos
//
//  Created by Fereizqo Sulaiman on 13/04/21.
//

import UIKit

class DetailCloudPhotosViewController: UIViewController {

    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var pictureImage: UIImageView!
    
    var delegateCloudPhotoVC: CloudPhotosViewController?
    var cloudPhoto: CloudPhoto?
    var useInteractiveDismiss = false
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Setup scroll view
        setupImageScrollView()
        
        // Setup tap gesture
        setupTapGesture()
        
        // Setup pan gesture
        setupPanGesture()
        
        // Setup dismiss button appearances
        dismissButton.layer.cornerRadius = dismissButton.frame.height/2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Do any additional setup when view is about to be added to a view hierarchy.
        showHUD(false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Do any additional setup when view is about to be dismissed to a view hierarchy.
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        // Do any additional setup when view is about to layout its subviews.
        setZoomForSize(scrollView.bounds.size)
    }
    
    // MARK: - ImageScrollView Setup
    
    func setupImageScrollView() {
        // Null safety for cloud photo data
        guard let photo = cloudPhoto else { return }
        
        // Setup and Load image in imageview
        pictureImage.contentMode = .scaleAspectFit
        pictureImage.load(urlString: photo.url)
        pictureImage.clipsToBounds = true
        
        // Setup scrollView and adjust image isze
        scrollView.delegate = self
        scrollView.contentInsetAdjustmentBehavior = .never
        if let imageSize = pictureImage.image?.size {
            scrollView.contentSize = imageSize
            setZoomForSize(scrollView.bounds.size)
        }
    }
    
    func setZoomForSize(_ scrollViewSize: CGSize) {
        // Adjust scroll and image zoom
        let imageSize = pictureImage.bounds.size
        let widthScale = scrollViewSize.width / imageSize.width
        let heightScale = scrollViewSize.height / imageSize.height
        let minScale = min(heightScale, widthScale)
        
        scrollView.minimumZoomScale = minScale
        scrollView.maximumZoomScale = 3.0
        scrollView.zoomScale = minScale
    }
    
    // MARK:- Setup Tap Gesture
    private func setupTapGesture() {
        // Adding tap gesture to ScrollView
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(gesture:)))
        scrollView.addGestureRecognizer(tapGesture)
    }
    
    
    @objc private func handleTap(gesture: UITapGestureRecognizer) {
        // Create tap gesture
        
        // Zoom action
        if scrollView.zoomScale != 1.0 {
            UIView.animate(withDuration: 0.3) {
                self.scrollView.zoomScale = 1.0
                self.showHUD(true)
            }
        }
        // Single tap action
        else {
            dismissButton.alpha < 1.0 ? showHUD(true) : showHUD(false)
        }
    }
    
    private func showHUD(_ isVisible: Bool) {
        // Show HUD (Dismiss button)
        
        UIView.animate(withDuration: 0.3) {
            let alphaValue:CGFloat = isVisible ? 1.0 : 0.0
            self.dismissButton.alpha = alphaValue
        }
    }
    
    @IBAction func dismissButtonTap(_ sender: UIButton) {
        // Dismiss full screen
        if let delegate = transitioningDelegate as? TransitionDelegate {
            delegate.useInteractiveDismiss = false
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func bookmarkButtonTap(_ sender: UIButton) {
        // Bookmark current cloud photo data
        
        // Null safety for delegate and clouud photo data
        guard let delegate = delegateCloudPhotoVC, let photo = cloudPhoto else { return }
        // Save the current isBookmarked value
        let isBookmarked = photo.isBookmarked
        // Change the value reversely in cloud photo Data
        cloudPhoto!.isBookmarked = !isBookmarked
        // Change bookmark button appearances
        bookmarkButton.tintColor = cloudPhoto!.isBookmarked ? UIColor.systemTeal : .lightGray
        
        // Updating bookmark data in main screen (CloudPhotoVC)
        delegate.updateBookmarkData(photo: photo)
    }
}

extension DetailCloudPhotosViewController: UIScrollViewDelegate {
    // Scrollview Delegate
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        // View for zooming is the Image
        return pictureImage
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        // When zoom, dismiss HUD
        if !dismissButton.isHidden {
            showHUD(false)
        }
    }
}

//MARK:- Pan Gesture
extension DetailCloudPhotosViewController{
    private func setupPanGesture(){
        // Adding pan gesture to scroll view
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan(gesture:)))
        scrollView.addGestureRecognizer(panGesture)
    }
    
    // Will try to use this as interactive
    @objc func handlePan(gesture: UIPanGestureRecognizer){
        // Only use handle pan when zoomScale is 1
        guard scrollView.zoomScale == 1.0 else { return }
        
        guard transitionCoordinator == nil else { return }
        
        if gesture.state == .began {
            
            if let delegate = transitioningDelegate as? TransitionDelegate {
                // Set useInteractiveDismiss to true and viewWillDisappear will hide the image. This would create a smoother animation
                useInteractiveDismiss = true
                delegate.useInteractiveDismiss = true
                delegate.panGesture = gesture
                delegate.dismissVelocity = getGestureVelocity(panGesture: gesture)
                dismiss(animated: true)
                
                // Hide the HUD
                showHUD(false)
                
                beginInteractiveTransitionIfPossible(gesture: gesture)
            } else if gesture.state == .changed {
                beginInteractiveTransitionIfPossible(gesture: gesture)
            }
        }
    }
    
    func beginInteractiveTransitionIfPossible(gesture: UIPanGestureRecognizer){

        transitionCoordinator?.animate(alongsideTransition: nil, completion: { (context) in

            if (context.isCancelled && gesture.state == .changed) {
                if let delegate = self.transitioningDelegate as? TransitionDelegate {
                    delegate.dismissVelocity = self.getGestureVelocity(panGesture: gesture)
                    self.dismiss(animated: true, completion: nil)
                    self.beginInteractiveTransitionIfPossible(gesture: gesture)
                }
            }
        })
    }
    
    private func getGestureVelocity(panGesture: UIPanGestureRecognizer) -> CGFloat{
        // Get the direction by checking the sign of y in velocity
        let velocity = panGesture.velocity(in: scrollView)
        return velocity.y
    }
}
