//
//  CloudPhotos.swift
//  CloudPhotos
//
//  Created by Fereizqo Sulaiman on 12/04/21.
//

import Foundation

struct CloudPhoto: Codable {
    let id: String
    let url: String
    let category: Int
    let category_name: String
    var isBookmarked: Bool
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(String.self, forKey: .id) ?? "0"
        self.url = try container.decodeIfPresent(String.self, forKey: .url) ?? "www.google.com"
        self.category = try container.decodeIfPresent(Int.self, forKey: .category) ?? 0
        self.category_name = try container.decodeIfPresent(String.self, forKey: .category_name) ?? "even"
        self.isBookmarked = try container.decodeIfPresent(Bool.self, forKey: .isBookmarked) ?? false
    }
}
