# Cloud Photos App #
an iOS App that using web services to collect photos into our devices.

![Image of Screenshot](CloudPhotosImage/Screenshot.png)
***

## Features and Usage ##

Here is the parts of Cloud Photos App screen:

![Image of PartsOfApps](CloudPhotosImage/PartsOfApp.png)

The apps show the photos in list view where each row show the following content:

![Image of Row](CloudPhotosImage/RowContent.png)

There are several main features that we can use within this app, namely:

#### Search by Name ####
We can search a photo according to its name by using the Search Bar that located on Top of the Apps Screen.

#### Filter by Category ####
We are also able to categorize the photo based on its category by tapping Filter Button that located near Search Bar. 

#### See Detail of Image ####
To see more detail of the photo, just tap the Row of Photo List View that we wanted to see and the photo become fullscreen.

#### Bookmarks ####
Bookmarking the photo by Tapping the Bookmark sign in List View (located in Right Side) or in Image Detail (located in Bottom Side).

***

## Installation ##
There are two methods that can be used to install "Cloud Photos" app into our devices:

1. TestFlight
1. Xcode

### TestFlight ###
The easiest method to install is using Testlight. Testflight is an Apple product that makes it easy to invite users to test an apps before we release them to the App Store. The step is:

* Tap the [TestFlight Invitation Link](https://testflight.apple.com/join/t71tbWjO). Tap Accept to Install the Apps

![Image of Screenshot](CloudPhotosImage/AcceptTestFlight.png)

* If you don't have the TestFlight, you will be prompted to get the TestFlight first from the App Store.

![Image of Screenshot](CloudPhotosImage/PromptTestFlight.png)



### Xcode ###
You can download this repository and run in the XCode with the following steps:

* Download the repository and Extract the files. Then Open "CloudPhotos.xcworkspace".

![Image of Row](CloudPhotosImage/CloudPhotosXcodeFiles.png)

* Run in your XCode by tapping Play button in Toolbar that located at Top side of XCode and make it sure you already select the simulator device.

![Image of Row](CloudPhotosImage/PlayXcode.png)

***
### Contact ###
Fereizqo Ahmad Sulaiman

fereizqoahmad@gmail.com
